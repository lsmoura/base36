![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/lsmoura/base36.svg?style=flat-square)

# base36

Convert numbers to and from [Base36 format](https://en.wikipedia.org/wiki/Base36).

The package exports two functions: `base36encode` and `base36decode`

* base36encode takes one parameter and it should be a non-negative integer or a string that parses to one.
* base36decode takes one parameter and it should be a string.

## usage

```javascript
const base36 = require('base36');

const encodedValue = base36.base36encode(442); // encodedValue will have the string 'ca'
const decodedValue = base36.base36decode('ca'); // decodedValue will have the number 442
```

## license

MIT

## author

Sergio Moura