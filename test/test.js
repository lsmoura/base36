const assert = require('assert');

const { base36encode, base36decode } = require('../src');

const numberMap = {
  1234: 'ya',
  12: 'c',
  34: 'y',
  69397560: '15bfi0',
};

describe('number -> base36', () => {
  Object.keys(numberMap).forEach(n => {
    it(`${n}`, () => {
      assert.equal(base36encode(n), numberMap[n]);
    });
  });
});

describe('base36 -> number', () => {
  Object.keys(numberMap).forEach(n => {
    const v = numberMap[n];
    it(`${v}`, () => {
      assert.equal(base36decode(v), n);
    });
  });
});
